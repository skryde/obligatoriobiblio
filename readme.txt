Nombre: Sebastián Rodriguez
Correo: sebastianrodriguez@antel.com.uy

> Recomiendo leer el contenido del archivo README.md en https://gitlab.com/skryde/obligatoriobiblio.git

## Respecto al diseño de la solucion

* Al final del punto 9 del apartado `Requerimientos`, se indica: 'Cabe aclarar que el estado del socio se setea como "moroso".'; supongo que la idea era que cuando se hace la consulta de futuros morosos, se marca como moroso al socio que corresponda. Dado que la fecha por la que se consulta puede ser cualquiera, no me parecio correcto marcar la morosidad ya que puede haber casos en los que no corresponda (cuando se consulta una fecha en el futuro respecto a la actual, por ejemplo).

* Se creó un paquete llamado `controllers` con los módulos `copias.py`, `libros.py`, `prestamos.py` y `socios.py`; a los efectos de achicar el largo del modulo `views.py` donde suponen estar las vistas o controladores de la aplicación. Se mantuvieron las funciones de autenticacion y del index en el modulo `views.py`.


## Respecto a la autenticación

* El usuario del sistema se debe crear mediante el comando `createsuperuser` de django.


## Configuración del proyecto

* En el archivo `settings.py` del proyecto se debe registrar la middleware `'biblio.middlewares.is_logged_in_mddleware'`



## Respecto al funcionamiento de la UI

* Por temas de tiempo, no se pudieron implementar algunas cosas, por lo que se recomienda el uso de Google Chrome para visualizar de la mejor manera la aplicacion (por ejemplo, Firefox no soporte los campos del tipo `date` definidos en HTML5).

    > Para el caso particular de las fechas, en caso de utilizar Mozilla Firefox (u otro navegador que no soporte el campo `date`), las fechas deben ingresarse con el formato `Y-m-d`.


## Respecto a los modelos

* En el modelo `Libro` se consideró el `isbn` por un lado, y un `id` incremental por el otro para poder editar un libro cuyo `isbn` haya sido mal ingresado sin necesidad de borrar el libro y crear uno nuevo.

* En el modelo `Socio` se consideró el `documento` por un lado, y un `id` incremental por el otro para poder editar un socio cuyo `documento` haya sido mal ingresado sin necesidad de borrar el socio y crear uno nuevo.


## Respecto a los Socios

* No se valida que el documento sea valido, si se valida que el largo no sea mayor a 8 digitos.


## Respecto al Admin de Django

* Se registraron los modelos `Libro`, `CopiaLibro`, `Socio` y `Prestamo`.

* Para el modelo `CopiaLibro`:

    * Se deshabilitó la opción de eliminar.

* Para el modelo `PrestamoAdmin`:

    * Se deshabilitó la opción de eliminar.

    * Se deshabilitó la opción de agregar.
