from django.db import models


# Create your models here.
class Libro(models.Model):
    isbn = models.CharField(max_length=15, unique=True)
    titulo = models.CharField(max_length=150)
    autor = models.CharField(max_length=150)
    fecha_ingreso = models.DateField('fecha de ingreso', auto_now=True)

    codigo = models.CharField(max_length=50, null=True, default=None)

    def __str__(self):
        return '[{0}] {1} ({2})'.format(self.isbn, self.titulo, self.autor)


class CopiaLibro(models.Model):
    num_inventario = models.IntegerField(primary_key=True)
    estado = models.CharField(max_length=10, choices=[('p', 'prestado'), ('d', 'disponible')])

    # Claves foraneas
    isbn = models.ForeignKey(Libro, on_delete=models.CASCADE)

    def __str__(self):
        return '[{0}] {1} ({2})'.format(self.num_inventario, self.estado_string().capitalize(), self.isbn)

    def estado_string(self):
        if self.estado == 'p':
            return 'prestado'

        elif self.estado == 'd':
            return 'disponible'


class Socio(models.Model):
    documento = models.CharField(max_length=8, unique=True)
    nombre_completo = models.CharField(max_length=200)
    email = models.CharField(max_length=200, unique=True)
    fecha_nacimiento = models.DateField('fecha de nacimiento')
    estado = models.CharField(max_length=6, choices=[('m', 'moroso'), ('n', 'normal')])

    def __str__(self):
        return '[{0}] {1}'.format(self.documento, self.nombre_completo)

    def estado_string(self):
        if self.estado == 'm':
            return 'moroso'

        elif self.estado == 'n':
            return 'normal'


class Prestamo(models.Model):
    fecha_comienzo = models.DateField('fecha de comienzo')
    fecha_fin = models.DateField('fecha de fin', null=True)
    estado = models.CharField(max_length=9, choices=[('p', 'pendiente'), ('t', 'terminado')])

    # Claves foraneas
    copia_libro_id = models.ForeignKey(CopiaLibro, on_delete=models.CASCADE)
    socio_id = models.ForeignKey(Socio, on_delete=models.CASCADE)

    def __str__(self):
        return '{0} - {1}'.format(self.copia_libro_id.isbn, self.estado_string().capitalize())

    def estado_string(self):
        if self.estado == 'p':
            return 'pendiente'

        elif self.estado == 't':
            return 'terminado'
