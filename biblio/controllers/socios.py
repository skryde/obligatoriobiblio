import re

from django.db import IntegrityError
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.utils import dateparse, timezone
from django.views.decorators.http import require_http_methods

from biblio.models import Libro, Socio


# ABM Socio
@require_http_methods(["GET"])
def get_socio(req, id_socio=None):
    if id_socio:
        sc = get_object_or_404(Socio, pk=id_socio)
        asd = Libro.objects.filter(copialibro__prestamo__socio_id=sc).distinct()

        prestados = []
        for l in asd:
            prestados.append({
                'isbn': l.isbn,
                'titulo': l.titulo,
                'autor': l.autor
            })

        return JsonResponse({
            'error': False,
            'socio': {
                'id': sc.id,
                'documento': sc.documento,
                'nombre': sc.nombre_completo,
                'email': sc.email,
                'fecha_nac': sc.fecha_nacimiento,
                'estado': sc.estado_string().capitalize(),
                'libros_prestados': prestados
            }
        })

    else:
        socios = Socio.objects.all()

        if req.headers.get('x-requested-with') == 'XMLHttpRequest':
            tmp_list = []
            for s in socios:
                tmp_list.append({
                    'id': s.id,
                    'documento': s.documento,
                    'nombre': s.nombre_completo,
                    'email': s.email,
                    'fecha_nac': s.fecha_nacimiento,
                    'estado': s.estado_string().capitalize()
                })

            return JsonResponse({'socios': tmp_list})

        else:
            return render(req, 'socios/index.html', {
                'socios': socios
            })


@require_http_methods(["POST"])
def create_socio(req):
    document = req.POST['document']
    name = req.POST['name']
    email = req.POST['email']
    birthday = req.POST['birthday']
    state = 'n'

    if document == '' or name == '' or email == '' or birthday == '' or state == '':
        return JsonResponse({
            'errror': True,
            'msg': 'Debes completar todos los campos!'
        })

    patt_email = re.compile(
        r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
        r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-011\013\014\016-\177])*"'  # quoted-string
        r')@(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?$', re.IGNORECASE)  # domain

    if not patt_email.match(email):
        return JsonResponse({
            'error': True,
            'msg': 'El E-Mail no cumple con el formato.'
        })

    try:
        soc = Socio()
        soc.documento = document
        soc.nombre_completo = name
        soc.email = email
        soc.fecha_nacimiento = birthday
        soc.estado = state
        soc.save()

        return JsonResponse({
            'error': False,
            'msg': 'Socio registrado!'
        })

    except IntegrityError as e:
        return JsonResponse({
            'error': True,
            'msg': 'Ya existe un socio con el documento ingresado: {}'.format(e)
        })


@require_http_methods(["POST"])
def edit_socio(req, sid):
    document = req.POST['document']
    name = req.POST['name']
    email = req.POST['email']
    birthday = req.POST['birthday']

    if document == '' or name == '' or email == '' or birthday == '':
        return JsonResponse({
            'errror': True,
            'msg': 'Debes completar todos los campos!'
        })

    patt_email = re.compile(
        r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
        r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-011\013\014\016-\177])*"'  # quoted-string
        r')@(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?$', re.IGNORECASE)  # domain

    if not patt_email.match(email):
        return JsonResponse({
            'error': True,
            'msg': 'El E-Mail no cumple con el formato.'
        })

    if len(document) > 8:
        return JsonResponse({
            'error': True,
            'msg': 'Se ha ingresado un documento muy largo. El documento debe tener como máximo 8 digitos.'
        })

    soc = get_object_or_404(Socio, pk=sid)

    soc.documento = document
    soc.nombre_completo = name
    soc.email = email
    soc.fecha_nacimiento = birthday
    soc.estado = 'n'
    soc.save()

    return JsonResponse({
        'error': False,
        'msg': 'Socio editado!'
    })


@require_http_methods(["POST"])
def delete_socio(req, sid):
    soc = get_object_or_404(Socio, pk=sid)

    soc.delete()

    return JsonResponse({
        'error': False,
        'msg': 'Socio eliminado!'
    })


# Lista de morosos
@require_http_methods(['GET'])
def get_moroso(req):
    soc_morosos = Socio.objects.filter(estado='m').all()

    return render(req, 'socios/morosos.html', {
        'socios': soc_morosos
    })


# Futuros morosos
@require_http_methods(['GET'])
def moroso_por_fecha(req, fecha=None):
    if fecha:
        param_date = dateparse.parse_date(fecha)
        asd = param_date - timezone.timedelta(7)
        morosos = Socio.objects.filter(
            prestamo__estado='p',
            prestamo__fecha_comienzo__lt=asd
        ).all()

        today = timezone.now().date()
        print(param_date <= today)
        print(asd)
        tmp = []
        for m in morosos:
            tmp.append({
                'documento': m.documento,
                'nombre_completo': m.nombre_completo,
                'email': m.email,
                'fecha_nac': m.fecha_nacimiento,
                'estado': m.estado_string().capitalize()
            })

        return JsonResponse({
            'data': tmp
        })

    else:
        return render(req, 'socios/morosos_por_fecha.html')
