import re

from django.db import IntegrityError
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from django.views.decorators.http import require_http_methods

from biblio.models import CopiaLibro, Libro


# ABM Libro
@require_http_methods(["GET"])
def get_libro(req, isbn=None):
    if isbn:
        lbr = get_object_or_404(Libro, isbn=isbn)
        cop = CopiaLibro.objects.filter(isbn=lbr)

        copias = []
        for c in cop:
            copias.append({
                'nro_inventario': c.num_inventario,
                'estado': c.estado_string().capitalize()
            })

        return JsonResponse({
            'error': False,
            'libro': {
                'id': lbr.id,
                'codigo': lbr.codigo,
                'isbn': lbr.isbn,
                'titulo': lbr.titulo,
                'autor': lbr.autor,
                'fecha_ingreso': lbr.fecha_ingreso,
                'copias': copias
            }
        })

    else:
        libros = Libro.objects.all()

        if req.headers.get('x-requested-with') == 'XMLHttpRequest':
            tmp_list = []
            for l in libros:
                tmp_list.append({
                    'id': l.id,
                    'codigo': l.codigo,
                    'isbn': l.isbn,
                    'titulo': l.titulo,
                    'autor': l.autor,
                    'fecha_ingreso': l.fecha_ingreso
                })

            return JsonResponse({'libros': tmp_list})

        else:
            return render(req, 'libros/index.html', {
                'libros': libros
            })


@require_http_methods(["POST"])
def create_libro(req):
    code = req.POST['code']
    isbn = req.POST['isbn']
    title = req.POST['title']
    author = req.POST['author']

    if code == '' or isbn == '' or title == '' or author == '':
        return JsonResponse({
            'error': True,
            'msg': 'Debes completar todos los campos!'
        })

    patt_isbn = re.compile('^\d{10}$')

    if not patt_isbn.match(isbn):
        return JsonResponse({
            'error': True,
            'msg': 'El ISBN no cumple con el formato. El dato debe ser un numero de 10 digitos'
        })

    try:
        lib = Libro()
        lib.codigo = code
        lib.isbn = isbn
        lib.titulo = title
        lib.autor = author
        lib.fecha_ingreso = timezone.now()
        lib.save()

        return JsonResponse({
            'error': False,
            'msg': 'Libro registrado!'
        })

    except IntegrityError as e:
        return JsonResponse({
            'error': True,
            'msg': 'Ya existe un libro con el ISBN ingresado: {}'.format(e)
        })


@require_http_methods(["POST"])
def edit_libro(req, lid):
    code = req.POST['code']
    isbn = req.POST['isbn']
    title = req.POST['title']
    author = req.POST['author']

    if code == '' or isbn == '' or title == '' or author == '':
        return JsonResponse({
            'error': True,
            'msg': 'Debes completar todos los campos!'
        })

    patt_isbn = re.compile('^\d{10}$')

    if not patt_isbn.match(isbn):
        return JsonResponse({
            'error': True,
            'msg': 'El ISBN no cumple con el formato. El dato debe ser un numero de 10 digitos'
        })

    lbr = get_object_or_404(Libro, pk=lid)

    lbr.codigo = code
    lbr.isbn = isbn
    lbr.titulo = title
    lbr.autor = author
    lbr.save()

    return JsonResponse({
        'error': False,
        'msg': 'Libro editado!'
    })


@require_http_methods(["POST"])
def delete_libro(req, lid):
    lbr = get_object_or_404(Libro, pk=lid)

    lbr.delete()

    return JsonResponse({
        'error': False,
        'msg': 'Libro eliminado!'
    })
