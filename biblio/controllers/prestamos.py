from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from django.views.decorators.http import require_http_methods

from biblio.models import Prestamo, Socio, Libro, CopiaLibro


# ABM Prestamo
@require_http_methods(["GET"])
def get_prestamo(req, id_prestamo=None):
    if id_prestamo:
        pr = get_object_or_404(Prestamo, pk=id_prestamo)

        return JsonResponse({
            'error': False,
            'prestamo': {
                'fecha_comienzo': pr.fecha_comienzo,
                'fecha_fin': pr.fecha_fin,
                'estado': pr.estado_string().capitalize(),
                'copia_libro_id': {
                    'num_inventario': pr.copia_libro_id.num_inventario,
                    'estado': pr.copia_libro_id.estado_string().capitalize(),
                    'isbn': {
                        'isbn': pr.copia_libro_id.isbn.isbn,
                        'titulo': pr.copia_libro_id.isbn.titulo,
                        'autor': pr.copia_libro_id.isbn.autor,
                        'fecha_ingreso': pr.copia_libro_id.isbn.fecha_ingreso
                    }
                },
                'socio_id': {
                    'documento': pr.socio_id.documento,
                    'nombre_completo': pr.socio_id.nombre_completo,
                    'email': pr.socio_id.email,
                    'fecha_nacimiento': pr.socio_id.fecha_nacimiento,
                    'estado': pr.socio_id.estado
                }
            }
        })

    else:
        prstms = Prestamo.objects.all()

        return render(req, 'prestamos/index.html', {
            'prestamos': prstms
        })


@require_http_methods(['POST'])
def create_prestamo(req, id_socio=None, isbn=None):
    if id_socio and isbn:
        try:
            soc = get_object_or_404(Socio, id=id_socio)
            if soc.estado != 'n':
                return JsonResponse({
                    'error': True,
                    'msg': 'El socio seleccionado es moroso y no se pueden realizar prestamos a su nombre!'
                })

            lib = Libro.objects.filter(isbn=isbn).first()
            if not lib:
                return JsonResponse({
                    'error': True,
                    'msg': 'No existe un libro con ISBN {}'.format(isbn)
                })

            cop = CopiaLibro.objects.filter(isbn=lib, estado='d').first()
            if not cop:
                return JsonResponse({
                    'error': True,
                    'msg': 'No existe una copia del libro [{0}] {1} ({2}) disponible.'.format(lib.isbn,
                                                                                              lib.titulo, lib.autor)
                })

            cop.estado = 'p'
            cop.save()

            pr = Prestamo()
            pr.fecha_comienzo = timezone.now()
            pr.estado = 'p'
            pr.copia_libro_id = cop
            pr.socio_id = soc

            pr.save()

            return JsonResponse({
                'error': False,
                'msg': 'Préstamo registrado correctamente.'
            })

        except Exception as e:
            return JsonResponse({
                'error': True,
                'msg': 'Error: {}'.format(e)
            })

    else:
        return JsonResponse({
            'error': True,
            'msg': 'Debes completar todos los campos!'
        })


# Prestamos por fecha
@require_http_methods(['GET'])
def prestamo_por_fecha(req, fecha=None):
    if fecha:
        prestamos = Prestamo.objects.filter(fecha_comienzo=fecha).all()
        tmp = []
        for pr in prestamos:
            tmp.append(
                {
                    'libro': pr.copia_libro_id.isbn.titulo,
                    'socio': pr.socio_id.nombre_completo,
                    'fecha': pr.fecha_comienzo + timezone.timedelta(7),
                    'estado': pr.estado_string().capitalize()
                }
            )
        return JsonResponse({
            'data': tmp
        })

    else:
        return render(req, 'prestamos/por_fecha.html')


# ABM Devolucion
@require_http_methods(['POST'])
def create_devolucion(req, id_socio, nro_inventario):
    soc = get_object_or_404(Socio, pk=id_socio)
    cop = get_object_or_404(CopiaLibro, pk=nro_inventario)

    try:
        cop.estado = 'd'
        cop.save()

        pr = cop.prestamo_set.filter(copia_libro_id=cop, socio_id=soc, estado='p').get()
        pr.estado = 't'
        pr.fecha_fin = timezone.now().date()
        pr.save()

        msg = "Libro devuelto."

        if pr.fecha_fin - pr.fecha_comienzo > timezone.timedelta(7):
            soc.estado = 'm'
            soc.save()
            msg += " Socio marcado como Moroso."

        return JsonResponse({
            'error': False,
            'msg': msg
        })

    except Exception as e:
        return JsonResponse({
            'error': True,
            'msg': 'Error: {}'.format(e)
        })
