from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.views.decorators.http import require_http_methods

from biblio.models import CopiaLibro, Libro, Socio


# ABM CopiaLibro
@require_http_methods(["GET"])
def get_copia(req, inventario=None):
    if inventario:
        cop = get_object_or_404(CopiaLibro, pk=inventario)

        json = {
            'error': False,
            'copia': {
                'num_inventario': cop.num_inventario,
                'estado': cop.estado_string().capitalize(),
                'isbn': {
                    'isbn': cop.isbn.isbn,
                    'fecha_ingreso': cop.isbn.fecha_ingreso,
                    'titulo': cop.isbn.titulo,
                    'autor': cop.isbn.autor
                }
            }
        }

        if cop.estado == 'p':
            prestamo = cop.prestamo_set.filter(copia_libro_id=cop, estado='p').get()
            json['socio'] = {
                'documento': prestamo.socio_id.documento,
                'nombre_completo': prestamo.socio_id.nombre_completo,
                'email': prestamo.socio_id.email,
                'fecha_nacimiento': prestamo.socio_id.fecha_nacimiento,
                'estado': prestamo.socio_id.estado
            }

        return JsonResponse(json)

    else:
        copias = CopiaLibro.objects.all()

        return render(req, 'copias/index.html', {
            'copias': copias,
            'libros': Libro.objects.all()
        })


@require_http_methods(["POST"])
def create_copia(req):
    isbn = req.POST['isbn']
    estado = 'd'
    cant = req.POST['cantidad']

    if isbn == '' or cant == '':
        return JsonResponse({
            'error': True,
            'msg': 'Debes completar todos los campos!'
        })

    try:
        lib = get_object_or_404(Libro, isbn=isbn)

        for _ in range(int(cant)):
            clib = CopiaLibro()
            clib.isbn = lib
            clib.estado = estado
            clib.save()

        return JsonResponse({
            'error': False,
            'msg': 'Copias registradas!'
        })

    except Exception as e:
        return JsonResponse({
            'error': True,
            'msg': 'Error: {}'.format(e)
        })


@require_http_methods(["POST"])
def delete_copia(req, inventario):
    cop = get_object_or_404(CopiaLibro, pk=inventario)

    cop.delete()

    return JsonResponse({
        'error': False,
        'msg': 'Copia eliminada!'
    })
