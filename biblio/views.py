from django.contrib.auth import authenticate, login, logout
from django.views.decorators.http import require_http_methods
from django.views.generic import View
from django.shortcuts import render, redirect
from biblio.models import Libro, Socio, CopiaLibro


# Login
class LoginController(View):

    @staticmethod
    def get(req):
        return render(req, 'auth/login.html')

    @staticmethod
    def post(req):
        username = req.POST['username']
        passwd = req.POST['password']

        user = authenticate(req, username=username, password=passwd)
        if user is not None:
            login(req, user)
            return redirect('biblio:index')

        else:
            return render(req, 'auth/login.html', {
                'error': 'Usuario y/o contraseña incorrecta!',
                'old': {
                    'username': username
                }
            })


# Logout
@require_http_methods(['GET'])
def my_logout(req):
    logout(req)
    return redirect('biblio:login')


# Index
@require_http_methods(['GET'])
def index(req):
    return render(req, 'index.html', {
        'cant_libros': Libro.objects.count(),
        'cant_copias': CopiaLibro.objects.count(),
        'cant_libros_prestados': Libro.objects.filter(copialibro__estado='p').count(),
        'cant_socios': Socio.objects.count(),
        'cant_socios_morosos': Socio.objects.filter(estado='m').count()
    })
