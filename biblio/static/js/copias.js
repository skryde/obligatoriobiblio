var modalIsbn = $('#isbn');
var modalCant = $('#cant');

$('#btnCreate').click(function() {
    modalIsbn.val('');

    modalAcceptBtn.html('Registrar');

    editModal = false;
    urlPostfix = '/create';

    modal.modal('show')
});

$('#CreateOrEdit').submit(function(e) {
    var esto = this;
    e.preventDefault();

    var data = {
        csrfmiddlewaretoken: csrfToken,
        isbn: modalIsbn.val(),
        cantidad: modalCant.val()
    };

    var url = esto.action + urlPostfix;

    doAjax(url, data);
});

$('.btn-information').click(function(e) {
    var esto = this;
    e.preventDefault();

    var infoModal = $('#myInfoModal');

    // Informacion de la copia
    var infoModalNroInvField = $('#nroInv');
    var infoModalEstadoField = $('#estado');

    // Informacion del libro
    var infoModalIsbnField = $('#infoIsbn');
    var infoModalDateField = $('#infoDate');
    var infoModalTitleField = $('#infoTitle');
    var infoModalAuthorField = $('#infoAuthor');

    // Informacion del socio
    var infoModalDocumentField = $('#document');
    var infoModalFullNameField = $('#name');
    var infoModalEmailField = $('#email');
    var infoModalBirthDateField = $('#birthday');
    var infoModalStatusField = $('#state');

    var cId = $(esto).data('inventario');

    $.ajax({
        url: '/biblio/copia/' + cId,
        method: 'GET',
        success: function(response) {
            console.log(response);

            infoModalNroInvField.val(response.copia.num_inventario);
            infoModalEstadoField.val(response.copia.estado);
            infoModalIsbnField.val(response.copia.isbn.isbn);
            infoModalDateField.val(response.copia.isbn.fecha_ingreso);
            infoModalTitleField.val(response.copia.isbn.titulo);
            infoModalAuthorField.val(response.copia.isbn.autor);

            if(response.socio) {
                infoModalDocumentField.val(response.socio.documento);
                infoModalFullNameField.val(response.socio.nombre_completo);
                infoModalEmailField.val(response.socio.email);
                infoModalBirthDateField.val(response.socio.fecha_nacimiento);
                infoModalStatusField.val(response.socio.estado);

                $('.ifsocio').removeClass('hidden');
            } else {
                $('.ifsocio').addClass('hidden');
            }

            infoModal.modal('show');
        },
        error: function(response) {
            console.log(response)
        }
    });
});

$('.btn-delete').click(function(e) {
    var esto = this;
    e.preventDefault();

    var cId = $(esto).data('inventario');

    deleteElement('copia', cId)
});

$(document).ready(function() {
    //$('#myInfoModal').modal("show")
});
