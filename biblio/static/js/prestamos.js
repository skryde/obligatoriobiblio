$('#btnCreate').click(function() {
    var socioSelect = $('#socio');
    var libroSelect = $('#libro');

    socioSelect.empty();
    libroSelect.empty();

    $.ajax({
        url: '/biblio/libro',
        method: 'GET',
        success: function(libResp) {
            console.log(libResp);

            $.ajax({
                url: '/biblio/socio',
                method: 'GET',
                success: function(socResp) {
                    console.log(socResp);

                    socioSelect.append(new Option("Seleccione un socio...", ""));
                    $.each(socResp.socios, function(key, val) {
                        socioSelect.append(new Option("[" + val.documento + "]" + " " + val.nombre, val.id))
                    });

                    libroSelect.append(new Option("Seleccione un libro...", ""));
                    $.each(libResp.libros, function(key, val) {
                        libroSelect.append(new Option("[" + val.isbn + "]" + " " + val.titulo + " (" + val.autor + ")", val.isbn))
                    });

                    modal.modal('show');
                },
                error: function(response) {
                    console.log(response);
                }
            });
        },
        error: function(response) {
            console.log(response);
        }
    });
});

$('#CreateOrEdit').submit(function(e) {
    var esto = this;
    e.preventDefault();

    var socioId = $('#socio').val();
    var libIsbn = $('#libro').val();

    var data = {
        csrfmiddlewaretoken: csrfToken
        //socioId: socioId,
        //libIsbn: libIsbn
    };

    var url = esto.action + '/' + socioId + '/' + libIsbn;

    doAjax(url, data);
});

$('.btn-information').click(function(e) {
    var esto = this;
    e.preventDefault();

    var infoModal = $('#myInfoModal');

    // Informacion del prestamo
    var infoModalFechaInicioField = $('#fechaInicio');
    var infoModalFechaFinField = $('#fechaFin');
    var infoModalEstadoField = $('#estado');

    // Informacion del libro
    var infoModalIsbnField = $('#infoIsbn');
    var infoModalDateField = $('#infoDate');
    var infoModalTitleField = $('#infoTitle');
    var infoModalAuthorField = $('#infoAuthor');

    // Informacion del socio
    var infoModalDocumentField = $('#document');
    var infoModalFullNameField = $('#name');
    var infoModalEmailField = $('#email');
    var infoModalBirthDateField = $('#birthday');
    var infoModalStatusField = $('#state');

    // ID del prestamo
    var pId = $(esto).data('id');

    $.ajax({
        url: '/biblio/prestamo/' + pId,
        method: 'GET',
        success: function(response) {
            console.log(response);

            infoModalFechaInicioField.val(response.prestamo.fecha_comienzo);
            infoModalFechaFinField.val(response.prestamo.fecha_fin);
            infoModalEstadoField.val(response.prestamo.estado);
            infoModalIsbnField.val(response.prestamo.copia_libro_id.isbn.isbn);
            infoModalDateField.val(response.prestamo.copia_libro_id.isbn.fecha_ingreso);
            infoModalTitleField.val(response.prestamo.copia_libro_id.isbn.titulo);
            infoModalAuthorField.val(response.prestamo.copia_libro_id.isbn.autor);

            infoModalDocumentField.val(response.prestamo.socio_id.documento);
            infoModalFullNameField.val(response.prestamo.socio_id.nombre_completo);
            infoModalEmailField.val(response.prestamo.socio_id.email);
            infoModalBirthDateField.val(response.prestamo.socio_id.fecha_nacimiento);
            infoModalStatusField.val(response.prestamo.socio_id.estado);
        },
        error: function(response) {
            console.log(response)
        }
    });

    infoModal.modal('show');
});

$('.btn-devolucion').click(function(e) {
    var esto = this;
    e.preventDefault();

    var sId = $(esto).data('idsocio');
    var nInv = $(esto).data('nroinventario');

    doAjax('/biblio/devolucion/' + sId + '/' + nInv, { csrfmiddlewaretoken: csrfToken });
});
