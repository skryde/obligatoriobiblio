var modalIsbn = $('#isbn');
var modalCode = $('#code');
var modalTitle = $('#title');
var modalAuthor = $('#author');
var modalFechaIngreso = $('#date');

var tabla;

$('#btnCreate').click(function() {
    modalIsbn.val('');
    modalAuthor.val('');
    modalTitle.val('');
    modalFechaIngreso.val(getCurrentDate());

    modalAcceptBtn.html('Registrar');

    editModal = false;
    urlPostfix = '/create';

    modal.modal('show');
});

$('#CreateOrEdit').submit(function(e) {
    var esto = this;
    e.preventDefault();

    var data = {
        csrfmiddlewaretoken: csrfToken,
        isbn: modalIsbn.val(),
        code: modalCode.val(),
        title: modalTitle.val(),
        author: modalAuthor.val()
    };

    var url = esto.action + urlPostfix;

    doAjax(url, data);
});

$('.btn-information').click(function(e) {
    var esto = this;
    e.preventDefault();

    var modalInfoIsbn = $('#infoIsbn');
    var modalInfoCode = $('#infoCode');
    var modalInfoFechaIngreso = $('#infoDate');
    var modalInfoTitle = $('#infoTitle');
    var modalInfoAuthor = $('#infoAuthor');

    var lIsbn = $(esto).data('isbn');

    $.ajax({
        url: '/biblio/libro/' + lIsbn,
        method: 'GET',
        success: function(response) {
            console.log(response);

            modalInfoIsbn.val(response.libro.isbn);
            modalInfoCode.val(response.libro.codigo);
            modalInfoFechaIngreso.val(response.libro.fecha_ingreso);
            modalInfoTitle.val(response.libro.titulo);
            modalInfoAuthor.val(response.libro.autor);

            try {
                tabla.destroy();
            } catch(e) {
                console.log(e);
            }

            tabla = $('#table').DataTable({
                data: response.libro.copias,
                'columns': [
                    {data: "nro_inventario"},
                    {data: "estado"}
                ],
                'pagingType': 'full_numbers',
                'info': false,
                'language': {
                    'lengthMenu': 'Tamaño de la página: _MENU_',
                    'zeroRecords': 'No se encontraron resultados',
                    'emptyTable': 'Ningún dato disponible en esta tabla',
                    'search': 'Buscar: ',
                    'paginate': {
                        'first':    '<<',
                        'last':     '>>',
                        'next':     '>',
                        'previous': '<'
                    }
                }
            });

            $('#myInfoModal').modal();
        },
        error: function(response) {
            console.log(response)
        }
    });
});

$('.btn-edit').click(function(e) {
    var esto = this;
    e.preventDefault();

    var lId = $(esto).data('id');
    var lIsbn = $(esto).data('isbn');

    $.ajax({
        url: '/biblio/libro/' + lIsbn,
        method: 'GET',
        success: function(response) {
            console.log(response);

            modalIsbn.val(response.libro.isbn);
            modalCode.val(response.libro.codigo);
            modalAuthor.val(response.libro.autor);
            modalTitle.val(response.libro.titulo);
            modalFechaIngreso.val(response.libro.fecha_ingreso);

            modalAcceptBtn.html('Editar');

            editModal = true;
            urlPostfix = '/' + lId + '/edit';

            modal.modal('show');
        },
        error: function(response) {
            console.log(response)
        }
    });
});

$('.btn-delete').click(function(e) {
    var esto = this;
    e.preventDefault();

    var lId = $(esto).data('id');

    deleteElement('libro', lId);
});

$(document).ready(function() {
    modal.on('shown.bs.modal', function() {
        modalIsbn.focus();
    });
});
