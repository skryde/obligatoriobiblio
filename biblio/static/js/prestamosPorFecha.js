var fechaPrestamo = $('#fechaPrestamo');
var tabla = $('#table').DataTable({
    "pagingType": "full_numbers",
    'info': false,
    'language': {
        "lengthMenu": "Tamaño de la pagina: _MENU_",
        "zeroRecords": "No se encontraron resultados",
        "emptyTable": "Ningún dato disponible en esta tabla",
        "search": "Buscar:",
        "paginate": {
            "first":    "Primero",
            "last":     "Último",
            "next":     "Siguiente",
            "previous": "Anterior"
        }
    }
});

$('#searchButton').click(function(e) {
    e.preventDefault();
    console.log(fechaPrestamo.val());
    $.ajax({
        url: '/biblio/prestamo_fecha/' + fechaPrestamo.val(),
        method: 'GET',
        success: function(response) {
            console.log(response);
            tabla.destroy();
            tabla = $('#table').DataTable({
                data: response.data,
                'columns': [
                    {data: "libro"},
                    {data: "socio"},
                    {data: "fecha"},
                    {data: "estado"}
                ],
                'pagingType': 'full_numbers',
                'info': false,
                'language': {
                    'lengthMenu': 'Tamaño de la página: _MENU_',
                    'zeroRecords': 'No se encontraron resultados',
                    'emptyTable': 'Ningún dato disponible en esta tabla',
                    'search': 'Buscar: ',
                    'paginate': {
                        'first':    '<<',
                        'last':     '>>',
                        'next':     '>',
                        'previous': '<'
                    }
                }
            });
        },
        error: function(response) {
            console.log(response);
        }
    });
});
