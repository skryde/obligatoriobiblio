var modalDocument = $("#document");
var modalName = $("#name");
var modalEmail = $("#email");
var modalBirthday = $("#birthday");

var tabla;


$('#btnCreate').click(function() {
    modalDocument.val('');
    modalName.val('');
    modalEmail.val('');
    modalBirthday.val('');

    modalAcceptBtn.html('Registrar');

    editModal = false;
    urlPostfix = '/create';

    modal.modal('show');
});

$("#CreateOrEdit").submit(function(e) {
    var esto = this;
    e.preventDefault();

    var data = {
        csrfmiddlewaretoken: csrfToken,
        document: modalDocument.val(),
        name: modalName.val(),
        email: modalEmail.val(),
        birthday: modalBirthday.val()
    };

    var url = esto.action + urlPostfix;

    doAjax(url, data)
});

$('.btn-information').click(function(e) {
    var esto = this;
    e.preventDefault();

    var modalInfoDocument = $('#infoDocument');
    var modalInfoName = $('#infoName');
    var modalInfoEmail = $('#infoEmail');
    var modalInfoBirthday = $('#infoBirthday');
    var modalInfoState = $('#infoState');

    var sId = $(esto).data('id');

    $.ajax({
        url: '/biblio/socio/' + sId,
        method: 'GET',
        success: function(response) {
            console.log(response);

            modalInfoDocument.val(response.socio.documento);
            modalInfoName.val(response.socio.nombre);
            modalInfoEmail.val(response.socio.email);
            modalInfoBirthday.val(response.socio.fecha_nac);
            modalInfoState.val(response.socio.estado);

            try {
                tabla.destroy();
            } catch(e) {
                console.log(e);
            }

            tabla = $('#table').DataTable({
                data: response.socio.libros_prestados,
                'columns': [
                    {data: "isbn"},
                    {data: "titulo"},
                    {data: "autor"}
                ],
                'pagingType': 'full_numbers',
                'info': false,
                'language': {
                    'lengthMenu': 'Tamaño de la página: _MENU_',
                    'zeroRecords': 'No se encontraron resultados',
                    'emptyTable': 'Ningún dato disponible en esta tabla',
                    'search': 'Buscar: ',
                    'paginate': {
                        'first':    '<<',
                        'last':     '>>',
                        'next':     '>',
                        'previous': '<'
                    }
                }
            });

            $('#myInfoModal').modal();
        },
        error: function(response) {
            console.log(response)
        }
    });
});

$('.btn-edit').click(function(e) {
    var esto = this;
    e.preventDefault();

    var sId = $(esto).data('id');

    $.ajax({
        url: '/biblio/socio/' + sId,
        method: 'GET',
        success: function(response) {
            console.log(response);

            modalDocument.val(response.socio.documento);
            modalName.val(response.socio.nombre);
            modalEmail.val(response.socio.email);
            modalBirthday.val(response.socio.fecha_nac);

            modalAcceptBtn.html('Editar');

            editModal = true;
            urlPostfix = '/' + sId + '/edit';

            modal.modal('show')
        },
        error: function(response) {
            console.log(response)
        }
    });
});

$('.btn-delete').click(function(e) {
    var esto = this;
    e.preventDefault();

    var sId = $(esto).data('id');

    deleteElement('socio', sId);
});

$(document).ready(function() {
    modal.on('shown.bs.modal', function() {
        modalDocument.focus();
    });
});
