var modal = $('#myModal');
var modalAcceptBtn = $('#btnAccept');
var csrfToken = $('input[name=csrfmiddlewaretoken]').val();

var editModal;
var urlPostfix;

var mensajes = {
    success: function(msg) {
        swal({
            title: "Correeecto!",
            text: msg,
            type: 'success'
        }, function() {
            setTimeout(function() {
                location.reload();

            }, 300);
        });
    },
    error: function(msg) {
        swal({
            title: "Oooops!",
            text: msg,
            type: 'error'
        });
    }
};

function getCurrentDate() {
    var now = new Date();
    var yyyy = now.getFullYear();
    var MM = now.getMonth() + 1;
    var dd = now.getDate();

    if (dd < 10) { dd = '0' + dd }
    if (MM < 10) { MM = '0' + MM }

    return yyyy + '-' + MM + '-' + dd;
}

function doAjax(mUrl, formData, mMethod, mFuncs) {
    $.ajax({
        url: mUrl,
        method: mMethod === undefined ? 'POST' : mMethod,
        data: formData,
        success: (mFuncs !== undefined && mFuncs.hasOwnProperty('success')) ? mFuncs.success : function(response) {
            console.log(response);
            if (response.error) {
                mensajes.error(response.msg);

            } else {
                mensajes.success(response.msg)
            }
        },
        error: (mFuncs !== undefined && mFuncs.hasOwnProperty('error')) ? mFuncs.error : function(response) {
            console.log(response);
        }
    })
}

function deleteElement(elem, elemId) {
    swal({
        title: "Estas seguro?",
        text: "No será posible recuperar esta información!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, borrar!",
        closeOnConfirm: false

    }, function(){
        $.ajax({
            url: '/biblio/' + elem + '/' + elemId + '/delete',
            method: 'POST',
            data: {
                csrfmiddlewaretoken: csrfToken
            },
            success: function(response) {
                console.log(response);
                if (response.error) {
                    mensajes.error(response.msg);

                } else {
                    mensajes.success(response.msg)
                }
            },
            error: function(response) {
                console.log(response);
            }
        });
    });
}
