from django.contrib import admin
from biblio.models import Libro, CopiaLibro, Socio, Prestamo


# Register your models here.
class LibroAdmin(admin.ModelAdmin):
    list_display = ['isbn', 'titulo', 'autor', 'fecha_ingreso']


class CopiaLibroAdmin(admin.ModelAdmin):
    list_display = ['num_inventario', 'isbn_name', 'estado']
    fieldsets = [
        (None, {
            'fields': ['isbn', 'estado']
        })
    ]

    # Se define un campo 'isbn_name' para poder cambiar el texto de la columna que se muestra en la tabla.
    def isbn_name(self, obj):
        return "[{0}] {1} ({2})".format(obj.isbn.isbn, obj.isbn.titulo, obj.isbn.autor)
    isbn_name.short_description = 'Libro'

    def get_actions(self, request):
        # Disable delete
        actions = super(CopiaLibroAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        # Disable delete
        return False


class SocioAdmin(admin.ModelAdmin):
    list_display = ['documento', 'nombre_completo', 'fecha_nacimiento', 'email', 'estado']


class PrestamoAdmin(admin.ModelAdmin):
    readonly_fields = ['fecha_comienzo', 'fecha_fin', 'estado', 'copia_libro_id', 'socio_id']
    list_display = ['fecha_comienzo', 'fecha_fin', 'estado', 'copia_libro_id', 'socio_id']

    def get_actions(self, request):
        # Disable delete
        actions = super(PrestamoAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        # Disable delete
        return False

    def has_add_permission(self, request):
        # Disable addition
        return False


admin.site.register(Libro, LibroAdmin)
admin.site.register(CopiaLibro, CopiaLibroAdmin)
admin.site.register(Socio, SocioAdmin)
admin.site.register(Prestamo, PrestamoAdmin)
