from django.shortcuts import redirect


def is_logged_in_mddleware(get_response):

    def middleware(request):
        # Tupla que define las excepciones de este middleware...
        # Es decir, las rutas donde este middleware no debe actuar.
        ex = ('/biblio/auth/login',)

        if request.user.is_authenticated or request.path in ex:
            response = get_response(request)

        else:
            response = redirect('biblio:login')

        return response

    return middleware
