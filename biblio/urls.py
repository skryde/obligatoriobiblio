from django.urls import re_path as url, include
from biblio import views

import biblio.controllers.libros as librocontroller
import biblio.controllers.socios as sociocontroller
import biblio.controllers.copias as copiacontroller
import biblio.controllers.prestamos as prestamocontroller


app_name = 'biblio'

urlpatterns = [
    # Auth
    url(r'^auth/', include([
        url(r'^login$', views.LoginController.as_view(), name='login'),
        url(r'^logout$', views.my_logout, name='logout')
    ])),

    # Index
    url(r'^$', views.index, name='index'),

    # ABM Prestamo
    url(r'^prestamo$', prestamocontroller.get_prestamo, name='prestamos'),
    url(r'^prestamo/(?P<id_prestamo>\d+)$', prestamocontroller.get_prestamo),
    url(r'^prestamo/(?P<id_socio>\w+)/(?P<isbn>\w+)$', prestamocontroller.create_prestamo),

    # Prestamos por fecha
    url(r'^prestamo_fecha$', prestamocontroller.prestamo_por_fecha, name='prestamos_por_fecha'),
    url(r'^prestamo_fecha/(?P<fecha>\d{4}-\d{2}-\d{2})$', prestamocontroller.prestamo_por_fecha),

    # ABM Devolucion
    url(r'^devolucion/(?P<id_socio>\w+)/(?P<nro_inventario>\d+)$', prestamocontroller.create_devolucion),

    # ABM Socio
    url(r'^socio$', sociocontroller.get_socio, name='socios'),
    url(r'^socio/(?P<id_socio>\d+)$', sociocontroller.get_socio),
    url(r'^socio/create$', sociocontroller.create_socio),
    url(r'^socio/(?P<sid>\d+)/edit$', sociocontroller.edit_socio),
    url(r'^socio/(?P<sid>\d+)/delete$', sociocontroller.delete_socio),

    # ABM CopiaLibro
    url(r'^copia$', copiacontroller.get_copia, name='copias'),
    url(r'^copia/(?P<inventario>\d+)$', copiacontroller.get_copia),
    url(r'^copia/create$', copiacontroller.create_copia),
    url(r'^copia/(?P<inventario>\d+)/delete$', copiacontroller.delete_copia),

    # ABM Libro
    url(r'^libro$', librocontroller.get_libro, name='libros'),
    url(r'^libro/(?P<isbn>\d{10})$', librocontroller.get_libro),
    url(r'^libro/create$', librocontroller.create_libro),
    url(r'^libro/(?P<lid>\d+)/edit$', librocontroller.edit_libro),
    url(r'^libro/(?P<lid>\d+)/delete$', librocontroller.delete_libro),

    # Lista de morosos
    url(r'^morosos$', sociocontroller.get_moroso, name='morosos'),

    # Morosos por fecha
    url(r'^morosos_fecha$', sociocontroller.moroso_por_fecha, name='morosos_por_fecha'),
    url(r'^morosos_fecha/(?P<fecha>\d{4}-\d{2}-\d{2})$', sociocontroller.moroso_por_fecha)
]
